package SimpleFluids;

import net.minecraftforge.common.MinecraftForge;
import SimpleFluids.handler.BucketHandler;
import SimpleFluids.init.ModBlocks;
import SimpleFluids.init.ModItems;
import SimpleFluids.lib.ModInfo;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

/**
 * @author Gigabit101
 */

@Mod(modid = ModInfo.MODID, name = ModInfo.MODNAME, version = ModInfo.MODVERSION)
public class SimpleFluids 
{
	@Mod.EventHandler
	public static void preinit(FMLPreInitializationEvent event) {}

	@Mod.EventHandler
	public static void init(FMLInitializationEvent event) 
	{
		ModBlocks.init();

		ModItems.init();

		BucketHandler.INSTANCE.buckets.put(ModBlocks.BlockFluidRf,
				ModItems.bucketRf);
		MinecraftForge.EVENT_BUS.register(BucketHandler.INSTANCE);

		BucketHandler.INSTANCE.buckets.put(ModBlocks.BlockFluidEu,
				ModItems.bucketEu);
		MinecraftForge.EVENT_BUS.register(BucketHandler.INSTANCE);
	}

	@Mod.EventHandler
	public static void postinit(FMLPostInitializationEvent event) {}

}

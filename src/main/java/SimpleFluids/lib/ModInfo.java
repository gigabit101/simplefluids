package SimpleFluids.lib;

/**
 * @author Gigabit101
 */

public class ModInfo 
{

	public static final String MODNAME = "SimpleFluids";

	public static final String MODID = "SimpleFluids";

	public static final String MODVERSION = "1.0.0";
}

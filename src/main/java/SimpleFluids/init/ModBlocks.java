package SimpleFluids.init;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import SimpleFluids.block.BlockFluidRf;
import SimpleFluids.block.BlockFluidEu;
import SimpleFluids.lib.ModInfo;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * @author Gigabit101
 */

public class ModBlocks {

	public static Fluid FluidRf = new Fluid("fluidRf");
	public static Block BlockFluidRf;

	public static Fluid FluidEu = new Fluid("fluidEu");
	public static Block BlockFluidEu;

	public static void init() 
	{
		// Fluid RF
		FluidRegistry.registerFluid(FluidRf);
		BlockFluidRf = new BlockFluidRf(FluidRf, Material.water).setBlockName("fluidRf");
		GameRegistry.registerBlock(BlockFluidRf, ModInfo.MODID + "_"
				+ BlockFluidRf.getUnlocalizedName().substring(5));
		FluidRf.setUnlocalizedName(BlockFluidRf.getUnlocalizedName());

		// Fluid EU
		FluidRegistry.registerFluid(FluidEu);
		BlockFluidEu = new BlockFluidEu(FluidEu, Material.water).setBlockName("fluidEu");
		GameRegistry.registerBlock(BlockFluidEu, ModInfo.MODID + "_"
				+ BlockFluidEu.getUnlocalizedName().substring(5));
		FluidEu.setUnlocalizedName(BlockFluidEu.getUnlocalizedName());
	}

}

package SimpleFluids.init;

import net.minecraft.item.Item;
import SimpleFluids.item.ItemBucketEu;
import SimpleFluids.item.ItemBucketRf;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * @author Gigabit101
 */

public class ModItems {
	public static Item bucketRf;
	public static Item bucketEu;

	public static void init() {
		bucketRf = new ItemBucketRf(ModBlocks.BlockFluidRf);
		GameRegistry.registerItem(bucketRf, "fluidBucketRf");

		bucketEu = new ItemBucketEu(ModBlocks.BlockFluidEu);
		GameRegistry.registerItem(bucketEu, "fluidBucketEu");
	}

}

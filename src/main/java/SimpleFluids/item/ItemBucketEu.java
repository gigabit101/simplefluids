package SimpleFluids.item;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import SimpleFluids.lib.ModInfo;

/**
 * @author Gigabit101
 */

public class ItemBucketEu extends net.minecraft.item.ItemBucket 
{

	public ItemBucketEu(Block block) 
	{
		super(block);
		this.setUnlocalizedName("fluidEuBucket");
		this.setCreativeTab(CreativeTabs.tabDecorations);
	}

	@Override
	public void registerIcons(IIconRegister iconRegister) 
	{
		itemIcon = iconRegister.registerIcon(ModInfo.MODID.toLowerCase() + ":"
				+ getUnlocalizedName());
	}

}
